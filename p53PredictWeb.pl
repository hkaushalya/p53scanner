#! /usr/bin/perl

# this program is for making tata wt matrix
# For Web version Saran's Changes

open IN_1, 'P53MatConditional.txt';

$input  = "$ARGV[0]";      #input file
open(IN_2, $input) ||  die "$0: Couldn't open file $input for reading.\n";

$output  = "$ARGV[1]";      #input file
open(OUT, ">$output") || die "$0: Couldn't open file $output for writing.\n";

$logfile = "log".".out";
open(LOG, ">$logfile");

$TSSposition = $ARGV[2];  #enter the TSS position if known

#$/ = "\nTotal donor sites";
$/ = "***********************\n";
@nts = (a,c,g,t);

while (<IN_1>){
  if(/[acgt] /){
    ++$mat;
    #print "$mat  $_\n";
    @lines = split /\n/;
    foreach $line (@lines){
      if($line =~ /^[acgt]/m){
	($nt,@scs)=split(/\s+/,$line);
	$total_positions = @scs;
	for($ii=1;$ii<=$total_positions;++$ii){
	  if($scs[$ii-1] == 0){$scs[$ii-1]=1}
	  $score{$mat}{$nt}{$ii}=log($scs[$ii-1]/25);
	  #print "$mat  $nt  $ii  $score{$mat}{$nt}{$ii}\n";
	}
      }
    }
  }
}


$/ = "\n>";


$site_len = 10;
$start = 0;



while (<IN_2>) {
  $matrix = 1;
  $score =  $predSite = $somefound = 0;
  $confidence = "";
  ++$scan_count;
  # separate the lines in the input record
  #($header, $GeneSymbol, $seq) = split /\s+/;
  ($header,$seq) = split /\s+/;
  $header =~ s/\>//;
  $seqlen = length($seq);
  $motiftoprint = substr($seq,10,$seqlen-20);
  $seq =~ tr/ACGTN/acgtn/;
  $utroid = $seq;
  @utrnts = split(//,$utroid);
  #print OUT "@utrnts\n";
  $NsPosition = $seconNspos = $firstNspos = 0;
  $max = -100;
  $seq_len = length($seq);
  while ($seq =~ /nnnnnnnnnnnnnnnnnnnnnnnnnnnnnn{30,4000}/gi){
    $NsPosition = pos($seq);
    #print "Ns  $NsPosition \n";
    if($NsPosition < $TSSposition){
      $firstSub = substr($seq,0,$NsPosition);
      $firstNspos = $NsPosition;
    }
    elsif($NsPosition > $TSSposition){
      $seconNspos = $NsPosition;
      last;
    }
  }
  #print "$header  $firstNspos   $seconNspos\n";
  if($firstNspos > 0){
    $subLen = $seq_len - $firstNspos;
	$middleseq = substr($seq,$firstNspos,$subLen);
    $ntopackBefore = "n"x($firstNspos);
    $seq = $ntopackBefore.$middleseq;
}
  if($seconNspos > 0){
    $middleseq = substr($seq,0,$seconNspos);
    $ntopackAfter = "n"x($seq_len-$seconNspos);
    $seq = $middleseq.$ntopackAfter;
  }
  $newLen = length($seq);
  print LOG "new Length $newLen  Old $seq_len\n";
  ##print OUT "$header\n$seq\n";
  
  for($st = 0; $st<$seq_len-$site_len+1; ++$st){
    #      print "starting at st $st\n";
    $total_score = 0;
    for($ii = 1;$ii<=$site_len; ++$ii){
      $nt = $utrnts[$st+$ii-1];
      if(($nt eq "n") || ($nt eq "N")){last}
      $total_score += $score{$matrix}{$nt}{$ii};
      #	    print OUT "$nt  $st  $total_score\n";
     #print  OUT "$nt  $st  $ii $total_score $score{$matrix}{$nt}{$ii} \n";
    }
    $total_score = $total_score/$site_len;
    
    if($total_score > -0.5){
      $SITE = substr($seq,$st,10);
      if($SITE =~ /N+/){next}
      #print  OUT "$SITE   $total_score\n";
      @SiteNts = split(//,$SITE);
      if($SiteNts[3] ne 'c' && $SiteNts[6] eq 'g'){$matrix = 3;}
      elsif($SiteNts[3] eq 'c' && $SiteNts[6] ne 'g'){$matrix = 4;}
      elsif($SiteNts[3] eq 'c' && $SiteNts[6] eq 'g'){
	if($SiteNts[4] eq 'a' &&  $SiteNts[5] eq 'a'){$matrix = 5;}
	elsif($SiteNts[4] eq 'a' &&  $SiteNts[5] eq 't'){$matrix = 6;}
	elsif($SiteNts[4] eq 't' &&  $SiteNts[5] eq 'a'){$matrix = 7;}
	elsif($SiteNts[4] eq 't' &&  $SiteNts[5] eq 't'){$matrix = 8;}
	else{$matrix = 9;}
      }
      elsif($SiteNts[3] ne 'c' && $SiteNts[6] ne 'g'){$matrix = 10;}
      $Newsum = 0;
      for($ss=1;$ss<=10;++$ss){
	$nt = $SiteNts[$ss-1];
	$Newsum += $score{$matrix}{$nt}{$ss};
      }
      $mismatches = $Coremismatches = 0;
      for($ss=0;$ss<=2; ++$ss){
	if($SiteNts[$ss] eq 'a' || $SiteNts[$ss] eq 'g'){$SiteNts[$ss] =~ tr/ag/AG/;}
	else{++$mismatches}
      }
      if($SiteNts[3] eq 'c'){$SiteNts[3] = "C"}
      else{
	++$mismatches;
	++$Coremismatches;
      }
      for($ss=4;$ss<=5; ++$ss){
	if($SiteNts[$ss] eq 'a' || $SiteNts[$ss] eq 't'){$SiteNts[$ss] =~ tr/at/AT/;}
	else{
	  ++$mismatches;
	  ++$Coremismatches;
	}
      }
      if($SiteNts[6] eq 'g'){$SiteNts[6] = "G"}
      else{
	++$mismatches;
	++$Coremismatches;
      }
      for($ss=7;$ss<=9; ++$ss){
	if($SiteNts[$ss] eq 'c' || $SiteNts[$ss] eq 't'){$SiteNts[$ss] =~ tr/ct/CT/;}
	else{++$mismatches}
      }
      $SITE = join("",@SiteNts);
      $Newsum = $Newsum/10;
      $sitePOS = $st+1;
      if($Newsum > 0){
	++$predSite;
	$dist{$predSite} = $sitePOS;
	$SCORE{$predSite} = sprintf("%.2f",$Newsum);
	$motif{$predSite} = $SITE;
	$motifMisMatches{$predSite} = $mismatches;
	$CoreMis{$predSite} = $Coremismatches;
	#print "$nt $st\n";
      }
    }
  }
  for($ii=1; $ii<$predSite; ++$ii){
    for($kk=$ii+1; $kk<=$predSite; ++$kk){
      $diff = $dist{$kk} - $dist{$ii};
      if($diff < 10){next;}
      if($diff > 28 ){last;}
      $motifend = $dist{$kk}+9;
      $diff = $diff-10;
      #if(($SCORE{$ii} < 0) && ($SCORE{$kk} < 0)){next}
      if(($motif{$ii} =~ /n+/i)||($motif{$kk} =~ /n+/i)){next}
      $SumScore = $SCORE{$ii} + $SCORE{$kk};
      $mismatches= $motifMisMatches{$ii}+$motifMisMatches{$kk};
      $coremis = $CoreMis{$ii} + $CoreMis{$kk};
      if($mismatches > 6 || $coremis >2){next}
      if((($SumScore >= 0.9) && ($coremis == 0)) || (($SumScore > 1) && ($coremis <= 1))){
	++$somefound;
	$confidence = "High";
      }
      elsif(($SumScore >= 0.6) && ($coremis <= 1)){
	++$somefound;
	$confidence = "Medium";
      }
      else{
	++$somefound;
	$confidence = "Low";
      }
      $mismatches= $motifMisMatches{$ii}+$motifMisMatches{$kk};
      $coremis = $CoreMis{$ii} + $CoreMis{$kk};
      $motifbegin = $dist{$ii} - $TSSposition;
      $motifend = $motifend - $TSSposition;
      #print OUT "$dist{$ii} to $motifend  \t$motif{$ii}\($SCORE{$ii}\) \*$diff bp\* $motif{$kk}\($SCORE{$kk}\)    \t$confidence\n";
    #  print OUT "$header\t$GeneSymbol\t$motifbegin\t$motifend\t$motif{$ii}\t\($SCORE{$ii}\)\t$diff";
	  
	#  My Changes to Output
		
		print OUT "$motifbegin\t$motifend\t$motif{$ii}\t\($SCORE{$ii}\)\t$diff";
	#   print OUT "$header\t$motifbegin\t$motifend\t$motif{$ii}\t\($SCORE{$ii}\)\t$diff";
	#  changes end   
	   print OUT "bp\t$motif{$kk}\t\($SCORE{$kk}\)\t$confidence\t\($mismatches\)\n";
  }
  }
  
  if($somefound ==0){print OUT "   No sites predicted\n";}
#  print OUT "\-----------------------------------------------------------------------------------\n\n";
}

close IN_1;
close IN_2;
close OUT;
