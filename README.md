# README #

P53Scanner predicts potential P53 consensus binding sites. It implements a position weight matrix built from experimentally-characterized data and comparative genomics approaches. 